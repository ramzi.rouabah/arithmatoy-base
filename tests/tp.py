# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    resultat ="S"*n+ "0"
    return(resultat)

def S(n: str) -> str:
    if n.endswith("0"):
        n = "S" + n
    return n

def addition(a: str, b: str) -> str:
    NombreS = len(str(a)) + len(str(b)) - 2
    resultat = NombreS * "S" + "0"
    return resultat

def multiplication(a: str, b: str) -> str:
    NombreS = (len(str(a))-1)* (len(str(b))-1)
    resultat = NombreS * "S" + "0"
    return resultat


def facto_ite(n: int) -> int:
    result = 1
    if n < 0:
        return -1
    else:
        for i in range(1,n+1):
            result *= i
        return result


def facto_rec(n: int) -> int:
    if n <= 1:
        return 1
    else:
        return n*facto_rec(n-1)

def fibo_rec(n: int) -> int:
    if n <= 0:
        return 0
    if n <= 2:
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)

def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n <= 2:
        return 1

    nb1 = 1
    nb2 = 1
    result = 0
    for i in range(2,n):
        result = nb1 + nb2
        nb1,nb2 = nb2, result
    return result


def golden_phi(n: int) -> int:
    if n <= 0:
        return 0

    a, b = 0, 1
    for i in range(n):
        a, b = b, a + b
    
    return b / a


def sqrt5(n: int) -> int:
    #On cherche un nomnbre qui multiplié 5x par lui même est égal à n
    return 0.0 

def pow(a: float, n: int) -> float:
    result = 1
    for i in range(n):
        result = result*a
    return result
